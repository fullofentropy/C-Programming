# Data Types

Objects, functions, and expressions have a property called type, which are used to intrepret the binary value stored in an object or expression.

A variable's type regulates:
* The amount of memory occupied
* How the bit pattern stored is interpreted
* How a function's return value is to be interpreted

C "types" can be either **predefined** or **derived**.

## Predefined Types:
* Void
* Basic Types
    * Integer Types
    * Floating Types

*We will be going over derived types later*

---
## Data types:

| **keyword** | **description** | **example values** |
| :--- | :--- | :--- |
| **char** | one character in the local character set, represents characters as a decimal value stored as a small integer. | 'A', '$', 42, ( * ) |
| **int** | an integer | -31337, 0, 8338 |
| **float** | single-precision floating point (6 decimal places) | 3.14, 2.71828, 42.0 |
| **double** | double-precision floating point (15 decimal places) | 3.14159265359, 2.7182818284, 42.0 |
| **void** | no value is available |  |

---