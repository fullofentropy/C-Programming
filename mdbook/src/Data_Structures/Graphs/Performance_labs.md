
# Graphs Performance labs

1. Create an adjacency matrix of the following directed graph.

 (figure of graph for next two problems)

![](../../assets/trees1.png)

 
2. Give the depth-first traversal of the graph in the Figure above, starting from vertex A.


3. Give the breadth-first traversal of the graph in the Figure above, starting from vertex A.

(Figure of graph for next two problems)

![](../../assets/trees2.png)

4. Give the adjacency matrix representation of the graph 

5. Give the adjacency list representation of the graph 


6. A graph can be used to show relationships. For example, from the follow- ing list of people belonging to the same club (vertices) and their friend- ships (edges), find:
(figure for the problem)

![](../../assets/trees3.png)

a. all friends of John

b. all friends of Susan

c. all friends of friends of Jean 

d. all friends of friends of Jim
