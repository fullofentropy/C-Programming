
# IO Performance Labs

## (Hardware Inventory)
You’re the owner of a hardware store and need to keep an inventory that can tell you what tools you have, how many you have and the cost of each one. Write a program that initializes the file “hardware.dat” to 100 empty records, lets you input the data concerning each tool, enables you to list all your tools, lets you delete a record for a tool that you no longer have and lets you update any information in the file. The tool identification number should be the record number. Use the following information to start your file:

|Record #|Tool name|Quantity|Cost|
|-|-|-|-|
|3|Electric sander|7|57.98|
|17|Hammer|76|11.99|
|24|Jig saw|21|11.00|
|39|Lawn mower|3|79.50|
|56|Power saw|18|99.99|
|68|Screwdriver|106|6.99|
|77|Sledge hammer|11|21.50|
|83|Wrench|34|7.50|


## Write a program to copy one file to another, replacing all lowercase characters with their uppercase equivalents.

# Write a program that merges lines alternately from two files and writes the results to stdout. If one file has fewer lines than the other, the remaining lines from the larger file should simply be copied to stdout.

# Write a program that displays the contents of a file at the terminal 20 lines at a time. At the end of each 20 lines, have the program wait for a character to be entered from the terminal. If the character is the letter q, the program should stop the display of the file; any other character should cause the next 20 lines from the file to be displayed.

## (Outputting Type Sizes to a File)
Write a program that uses the sizeof operator to determine the sizes in bytes of the various data types on your computer system. Write the results to the file “datasize.dat” so you may print the results later. The format for the results in the file should be as follows (the type sizes on your computer might be different from those shown in the sample output):

|Data type|Size|
|-|-:|
|char|1|
|unsigned char|1|
|short int|2|
|unsigned short in|2|
|int|4|
|unsigned int|4|
|long int|4|
|unsigned long int|4|
|float|4|
|double|8|
|long double|16|