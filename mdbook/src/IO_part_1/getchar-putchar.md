# Basic I/O: getchar\(\) & putchar\(\)

## getchar\(\)

```c
int getchar(void)
```

* **Purpose:** Gets an unsigned char from stdin
* **Arguments:** None
* **Return Value:** This function returns the character read as an unsigned char cast to an int or EOF on end of file or error. 
* **Syntax Example:**

```c
int userInput = 0;                    // Will store input
printf("Enter a character: ");        // Prompts user
userInput = getchar();                // Stores user input into userInput
```

## putchar\(\)

```c
int putchar(int char)
```

* **Purpose:** Writes an unsigned char to stdout
* **Arguments:** Integer value of character to write
* **Return Value:** This function returns the character read as an unsigned char cast to an int or EOF on end of file or error.
* **Syntax Example:**

```c
printf("Your character was" );        // Prefaces output
putchar(userInput);                   // Prints output
```
---

## Complete Demonstration & Performance Labs