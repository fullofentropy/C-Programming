
## Debuggging Exercises
```c
	/* This program crashes.  The crash point and reason for crashing
	** can be quickly identified using a debugger.  */

#include <stdio.h>

main()
{
int x,y,z[3];

y=54389;
for (x=10; x>=1; x--)
  z[y]=y/x;
printf("%d\n",z[0]);
}

```
```c
	/* This program goes into an infinite loop.  It can be paused
	** mid-execution in a debugger to see what is happening. */

#include <stdio.h>

main()
{
int x,y;

y=0;
for (x=0; x<10; x++)
  {
  y=y+x;
  if (y > 10)
    x--;
  }
}

```
```c

	/* This program works partially, but then displays the wrong
	** values after some data is input.  A debugger can be used
	** to break at the problem area and step through the code line
	** by line to find the problem.  */

#include <stdio.h>

main()
{
int choice;
float ppg,rpg;

ppg=rpg=0.0;
choice=0;
do
  {
  printf("(1) Enter points per game\n");
  printf("(2) Enter rebounds per game\n");
  printf("(3) Quit\n");
  scanf("%d",&choice);
  if (choice == 1 || choice == 2)
    {
    printf("Amount: ");
    if (choice = 1)
      scanf("%f",&ppg);
    else if (choice == 2)
      scanf("%f",&rpg);
    printf("Points=%f Rebounds=%f\n",ppg,rpg);
    }
  }
while (choice != 3);
}

```c

	/* This program is supposed to find sequences of repeating letters.
	** On sequences of 2 letters it works fine; on 3 or more letters it
	** prints redundant lines of output.
	**
	** The debugger can be used to step through a loop, one line
	** at a time, to observe what the code is doing.  */

#include <stdio.h>

main()
{
char word[80];
int i,j;

printf("Enter any word: ");
scanf("%s",word);
i=0;
while (word[i] != '\0')
  {
  if (word[i] == word[i+1])
    {
    j=1;
    while (word[i] == word[i+j])
      j++;
    printf("%d consecutive %c\n",j,word[i]);
    }
  i++;
  }
}

```






