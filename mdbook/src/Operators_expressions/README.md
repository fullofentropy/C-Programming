# Operators and Expressions

---
### Slides for this section are available [here](https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Operators_expressions/slides/#/)


## Topics:

* ### Definitions
* ### Arithmetic Operators
* ### Relational Operators
* ### Logical Operators
* ### Assignment Operators
* ### Precedence

## By the end of this lesson you should know:

* What an Operand, Operator and Expression is
* How to utilize Arithmetic Operators
* How to utilize Relational Operators
* How to utilize Logical Operators
* How to utilize Assignment Operators
* The precedence of operators

---
