# The Little Int that Could

```c
int * find_smallest_natrual_num (int * intArr_ptr, int arrSize);
```

**Return Value:**
- Int pointer to the smallest natural number found in the array

**Parameters:**
- intArr_ptr - Pointer to an array of ints
- arrSize - Length of the array

**Purpose** - Find smallest natural number

**Requirements**
- Return NULL if intArr_ptr is NULL
- Return NULL if arraySize is unrealistic

**NOTE:** For this lab, a natural numbwer is a whole, positive number > 0
