```c
// TODO: Include needed headers
#include <stdio.h>  // For things such as printf
#include <string.h> // For things such as strcpy, memmove, etc
#include <stdlib.h> // For things such as malloc
int main(void)
{
    char string[] = "Hello World";
    char substring[] = "World";
    char substring2[] = "Wo";
    // ============== malloc() ===============
    printf("\n\n========= malloc() =========\n");
    // TODO: Create a buffer large enough to fit the string length +1 of string using malloc()
    // NOTE: strlen only gives us the string length. The +1 is for the nul-terminator
    char *buffer = malloc(strlen(string) + 1);
    // TODO: Check that buffer was created, if not return 1
    if (!buffer)
    {
        return 1;
    }
    // ============== strcpy() ===============
    printf("\n\n========= strcpy() =========\n");
    // TODO: Using strcpy(), copy the string into the buffer
    strcpy(buffer, string);
    printf("Buffer is now: %s\n\n", buffer);
    printf("String is still: %s\n", string);
    // ============== strcmp() ===============
    printf("\n\n========= strcmp() =========\n");
    // TODO: Compare string to buffer. Print out match or not match depending on return
    if (strcmp(string, buffer) == 0)
    {
        printf("Match!\n");
    }
    else
    {
        printf("Not a match!\n");
    }
    // ============== memcpy() ===============
    printf("\n\n========= memcpy() =========\n");
    // TODO: Create a char buffer on the stack of size 50 and zeroize
    char stackBuffer[50] = {0};
    printf("String in buffer before memcpy: %s\n", stackBuffer);
    // TODO: Using memcpy, copy the string from string into the new buffer
    memcpy(stackBuffer, string, strlen(string) + 1);
    printf("String is: %s\n", string);
    printf("String in buffer after memcpy: %s\n",stackBuffer);
    // ============== memmove() ===============
    printf("\n\n========= memmove() =========\n");
    // TODO: Create another buffer on the stack size 50 and zeroize
    char stackBuffer2[50] = {0};
    printf("String in buffer before memmove: %s\n", stackBuffer2);
    // TODO: Using memove, move the original string into the new buffer
    memmove(stackBuffer2, string, strlen(string) + 1);
    printf("String is: %s\n", string);
    printf("String in buffer after memcpy: %s\n",stackBuffer2);

// NOTE
    /*
    Weird eh? It looks like memcpy and memmove do the same thing. Well... they do. But they don't.
    memcpy() cannot handle overlapping memory. memmove on the other hand can. SO while the example above has no issue...
    the upcoming example MAY have issues with memcpy
    */
    memmove(&string[5], &string[6], 6);
    printf("%s\n", string);

    // NOTE: Do you see what we did there? Our destination and source were the same. We started at \
    the address of the 6th element in string 'W' and overwrote the same memory we were just copying from \
    aka removing " World" and effectivly replacing it with "World"... in other words removing the space between \
    the two words.

    // ============== strstr() ===============
    printf("\n\n========= strstr() =========\n");
    // TODO: Let's grab a substring from the String

    // TODO: Using strstr, find the substring variable within the string...\
    assign this new pointer position to a char pointer called mark_position
    char *mark_position = strstr(string, substring);
    printf("%s", mark_position);

    // TODO: Free your heap buffers
    free(buffer);

    getchar();
    getchar();
    return 0;
}    
```