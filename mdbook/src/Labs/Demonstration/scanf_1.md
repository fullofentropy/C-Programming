# Demonstration Lab 4

## Formatted I/O

**Formatted Input \(Strings\)**

* Read a string into a char array
* Specify a field-width to protect against buffer overflow
* Ensure the field-width leaves room for the nul-terminator 
* Stop reading at the first capital letter or new line
