# Greetings, Universe

KSAT: k0756

We just set up our environment, let's compile our first program.

1. Write the below program into VSCode
1. Compile and run the program in VSCode
1. Compile and run the program from your command line using gcc

```c
// Hello World is thinking too small. We need ambition!
// We want to greet the enitre Universe

#include <stdio.h>

int main() {
	printf("Greetings, Universe!");

	return 0;
}
```