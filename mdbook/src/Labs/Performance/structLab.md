# Struct Lab
## Surfin' Bird

**Define a struct that contains:**
- char sentence
- char searchWord
- int index
- int errorCode

**Use this function to create a new SurfinBird:**

`struct SurfinBird surfString(char sentence,`
` char searchWord);`

Output will be the index of the first 

Here are some error codes for this function. This should be stored under errorCode in the returned struct:

0. No Error
1. sentence is null
2. searchWord is null
3. searchWord is not found in sentence

Your function should print the human-readable output and return the struct.

## Sample Input

### Example One
```
("Once upon a time there was a pointer.", "pointer")
```

### Example Two
```
("Once upon a time there was a pointer.", "Goldfish")
```

## Sample Output
### Example One
```
Sentence: Once upon a time there was a pointer.
Search Word: pointer
Index of Search Word: 29
Error Code: 0 
```

### Example Two
```
Sentence: Once upon a time there was a pointer.
Search Word: Goldfish
Index of Search Word: -1
Error Code: 3
```

**Remember:** This function will both print the human-readable output AND return a SurfinBird with the relevant data.