## Performance Lab: EVEN IT OUT

IF-ELSE statement:
* Input a number from the user.
* Determine if the number is odd or even utilizing the "mod" operator  (N00b-tier efficiency).
* Utilizing an IF-ELSE statement:
    * If the number is odd, multiply the number by 2, store the result in the original variable, and print the new number.
    * If the number is even SAY SO!!
* BONUS: Determine if the number is odd or even utilizing a **bitwise** operator instead (god-tier efficiency).
