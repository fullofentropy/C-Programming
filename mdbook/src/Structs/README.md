# Structs

### The slides for this section are available [here](https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Structs/slides/#/)

### This topic covers:

* Coding Style Guide
* Stub Code
* Definition
* Format
* Arrays of Structs
* Struct Visualization
* Linked Lists
* Function Pointers
* Circular Lists
