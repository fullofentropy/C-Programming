# Debugging Tools
## Valgrind
###  What is Valgrind?
Valgrind is a tool that helps find mistakes in memory management, like memory leaks,
reading/writing past the end of an allocated block of memory, or use of an uninitialized value.

### Why should I care about Valgrind?
Valgrind can help you find many bugs in your code related to memory allocation. Some of these
bugs will cause your program to have the wrong output, or crash from a segmentation fault,
while others cause only intermittent issues (ie. printing of garbage values in uninitialized arrays)
or are not detectable. Valgrind’s descriptive output can be extremely helpful in locating and
understanding these bugs.
Also, the public and private tests will run Valgrind on your program, and you will lose points if
Valgrind finds errors or memory leaks, so you should make testing with Valgrind part of your
workflow.

### useful tools with Valgrind 

A number of useful tools are supplied as standard.

* Memcheck is a memory error detector. It helps you make your programs, particularly those written in C and C++, more correct.
* Cachegrind is a cache and branch-prediction profiler. It helps you make your programs run faster.
* Callgrind is a call-graph generating cache profiler. It has some overlap with Cachegrind, but also gathers some information that Cachegrind does not.
* Helgrind is a thread error detector. It helps you make your multi-threaded programs more correct.
* DRD is also a thread error detector. It is similar to Helgrind but uses different analysis techniques and so may find different problems.
* Massif is a heap profiler. It helps you make your programs use less memory.
* DHAT is a different kind of heap profiler. It helps you understand issues of block lifetimes, block utilisation, and layout inefficiencies.
* SGcheck is an experimental tool that can detect overruns of stack and global arrays. Its functionality is complementary to that of Memcheck: SGcheck finds problems that Memcheck can’t, and vice versa..
* BBV is an experimental SimPoint basic block vector generator. It is useful to people doing computer architecture research and development.

