## When do I use an IF statement?

Whenever you have only ONE condition to act on (1).

```C
if (somethingIsTrue)
{
then_do_this_thing();			//one condition
}

/*i dont want to do anything if it's false */

```
### When do I use an IF-ELSE statement?


Whenever you only have TWO conditions to act on (2).

```c
if (somethingIsTrue)
{
    then_do_this_thing();			//condition one
}
else
{
    otherwise_do_something_else();	//condition two
}

/*used for binary input validation, and error checking */

```
    
### When do I use an ELSE-IF statement?

Whenever you have TWO OR MORE conditions to act on (2+).
    
```c
if (somethingIsTrue)
{
    then_do_this_thing();           //condition one
}
else if (somethingElseisTrue)
{
    then_do_this();                 //condition two
}
else
{
    otherwise_do_this_instead();	//condition three
}

```
    
### Why should I use a SWITCH instead of ELSE-IF?

1.Readability


```c
///switch////

switch (echoNumber)
{
    case 1:
            printf("one\n");
            break;
    case 2:
            printf("two\n");
            break;
    case 3:
            printf("three\n");
            break;
    default:
            printf("???\n");
            break:
}

///ELSE-IF///

if (echoNumber == 1)
{
    printf("one\n");
}
else if (echoNumber == 2)
{
    printf("two\n");
}
else if (echoNumber == 3)
{
    printf("three\n");
}
else
{
    printf("???\n");
}

```

2.Easier to group multiple cases

```c
///switch///

switch (primeNumber)
{
    case1:
    case2:
    case3:
    case5:
            printf("prime\n");
            break;
    case4:
    case6:
            printf("not\n");
            break;
    default:
            printf("???\n");
            beak;
}

///ELSE-IF///

if (primeNumber == 1
    || primeNumber == 2
    || primeNumber == 3
    || primeNumber == 5)
{
    printf("prime\n");
}
else-if (primeNumber == 4
        || primeNumber == 6)
{
    printf("not\n");
}
else
{
    printf("???\n");
}

```

3.Faster.../easier to optimize...

A switch generates a jump table in assembly which is generally faster (depends on use case, compiler and underlying architecture)

4.ELSE-IF does not allow "fall through".

```c
///switch///

switch (countToNum)
{
    case 3:
            printf("three\n");
    case 2:
            printf("two\n");
    case 1:
            printf("one\n");
    case 0:
            printf("launch!\n");
break;

    default:
            printf("???\n");
            break;
}

///ELSE-IF///

if (countToNum == 3)
{
    print("three\n");
    print("two\n");
    print("one\n");
    print("launch!!!\n");
}
else if (countToNum == 2)
{
    print("two\n");
    print("one\n");
    print("launch!!!\n");
}
else if (countToNum == 1)
{
    print("one\n");
    print("launch!!!\n");
}
else if (countToNum == 0)
{
    print("launch!!!\n");
}
else
{
    print("???\n");
}

```

### When should I use ELSE-IF instead of SWITCH?

1. large ranges of cases (switch does not translate well to large ranges).
```c
///IF-EELSE///

if (rngNum >= 1 && rngNum <= 20)
{
    printf(("%d\n"), rngNum); 
}
else
{
    printf("???\n");
}

///SWITCH///

switch (rngNum)
{
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:
    (etc...etc...)
    case 18:
    case 19:
    case 20:
            printf("%d\n", rngNum);
            break;
    default:
            printf("???\n");
            break;
}

```

2. Conditional expressions (switch only evaluates constants).
```c
///ELSE-IF///

int inputNumber = 0;
scanf("%d", &inputNumber);
printf("number is ");

if (inputNumber > 0)
{
    printf("positive\n");
}
else if (inputNumber < 0)
{
    printf("negative\n");
}
else
{
    printf("zero\n");
}

///SWITCH///
int inputNumber = 0;
scanf("%d", &inputNumber);
printf("Number is ");

switch (inputNumber)
{
    case > 0:
    etc...etc...
}
/*SWITCH will result in a compiling error!*/
```
3. Variable values (switch case values must be literals).
```c
///IF-ELSE///

char char1, char2;
scanf("%1c %1c", &char1, &char2);
printf("are they equal?\n");

if (char1 == char2)
{
    printf("yes\n");
}
else
{
    printf("no\n");
}

///SWITCH///

char char1, char2;
scanf("%1c %1c", &char1, &char2);
printf("are they equal?\n");

switch (char1)
{
    case char2:
    etc...etc...
}

/*SWITCH will result in a compiling error!*/
```
4. Floating point values (floats are technically literals but DONT utilize == or switch with floats).
```c
///IF-ELSE///
float inputDecimal;
scanf("%f", &inputDecimal);
printf("equal to 3.14?\n");

if (inputDecimal > 3.14)
{
    printf("nope\n");
}
else if (inputDecimal < 3.14)
{
    printf("nope\n");
}
else
{
    printf("yes\n");
}

///SWITCH///
float inputDecimal;
scanf("%f", &inputDecimal);
printf("equal to 3.14?\n");

switch (inputDecimal)
{
    case 3.14:
    etc...etc...
}
/*SWITCH will result in a compiling error!*/
```
