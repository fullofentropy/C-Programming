# Control Flow

---

### The slides for this topic are available [here](https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Control_flow/slides/#/).
## Topics:

* ### Statements and Blocks
* ### Conditional Statements
* ### Loops
* ### Nested Control Flow

#### NOTE: Most labs will be within the lessons themselves for this chapter
