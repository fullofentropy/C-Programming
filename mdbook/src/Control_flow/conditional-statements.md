# IF statement

* Create conditional jumps

* Tests an expression as TRUE or FALSE

* When the expression is TRUE, the code block will be executed

```c
///minimum if statement syntax///

if (expression)
{
statement1;
}

///basic if statement examples///

if (1 > 0)               //example 1
{
printf("All is alright with the world. \n");
}

if (1)                  //example 2
{
printf("1 evaluates to TRUE. \n");
}

if (i  > u)  		 //example 3
{
printf("i is greater than u. (pun_intended = FALSE) \n");
}

if (i && i == u)         //example 4
{
printf("apparently, this works and i evaluates to TRUE. \n");
}

```

NOTE: Do not leave if statements unwrapped, watch those { } !!

---

## PERFORMANCE LAB 11

