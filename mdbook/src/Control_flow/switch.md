# Switch Statements

Switch statements also create conditional jumps.

Tests an expression or variable against cases.

When the variable is equivalent to a case, the following statements will execute until a break.

Break statements, while optional, can be used to end cases.

A switch case that does not end in a break statement will "fall through" and execute the following cases.

NOTE: Case labels must meet the following criteria:

    -must be unique

    -ends with a COLON

    -have constants/constant expression

    -integral type (integer, character)

    -not a floating point number

    -a switch case should have at most one default label

    -default can be placed anywhere in the switch

    -break statement takes control out of the switch

    -two or more cases may share one break statement

    -nesting (switch within switch) is allowed

    -relational operators are NOT allowed in switch statement

    -Macro identifier are allowed as switch case label

    -const variable is allowed in switch case statements

    -empty switch case is allowed

Switch statements also create **conditional** jumps.

```c
///switch statement syntax///

switch (variable)       //variable is tested against values
{
    case constant1:     //if (variable == constant1)...
            statement1; //..{statement1;}
            break;      //optional; exits if case is met

    case constant2:     // if (variable == constant2)...
            statement2; //..{statement2;}
            break;      //optional; exits if case if met

    default:            //optional; if no case is met...
            statement3; //..{statement3;}
            break;      //optional; exits if case is met
}

///switch statement examples///

switch (binaryInput)
{
    case 1:
            printf("%d = TRUE \n", binaryInput);
            break;
    case 0:
            printf("%d = FALSE \n", binaryInput);
            break;
    default:
            printf("something has gone wrong! \n");
            break;
}
```

