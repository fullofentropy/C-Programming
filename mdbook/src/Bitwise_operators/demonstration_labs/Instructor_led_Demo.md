# Instructor Led Demos

1. Write a program to shift inputted data by two bits right.

 ```

void main()

{

int x,y;

clrscr();

printf(“Read The Integer from keyboard (x) :-”);

scanf(“%d”, &x);

x>>=2;

y=x;

printf(“The Right shifted data is = %d”,y);

}
```
OUTPUT:
```
Read The Integer from keyboard (x) :- 8

The Right shifted data is = 2
```
Before the execution of the program: The number entered through the keyboard is 8 and its corresponding binary number is 1 0 0 0.

| 0| 0| 0| 0| 0| 0| 0| 0| 0| 0| 0| 0| 1| 0| 0| 0|
|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01|0|

After execution of the program: As per the above-given program, the inputted data x is to be shifted by 2 bits right side. The answer in binary bits would be as follows:

|0 | 0| 0| 0| 0| 0| 0| 0| 0| 0| 0| 0| 0| 0| 1| 0|
|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01| 0|

Shifting two bits right means the inputted number is to be divided by 2s where s is the number of shifts i.e. in short y=n/2s, where n = number and s = the number of position to be shift.

As per the program cited above, Y=8/2E2 = 2.

2.Write a program to shift inputted data by three bits left.

 
```c
void main()

{

int x,y;

clrscr();

printf(“Read The Integer from keyboard (x) :-”);

scanf(“%d”, &x);

x<<=3;

y=x;

printf(“The Right shifted data is = %d”,y);

}
```
OUTPUT:
```
Read The Integer from keyboard (x) :- 2

The Left shifted data is = 16
```
Before the execution of the program: The number entered through the keyboard is 2 and its corresponding binary number is 1 0. The bits will be as follows:

|0 | 0| 0| 0| 0| 0| 0| 0| 0| 0| 0| 0| 0| 0| 1| 0|
|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01| 0|


After execution of the program: As per the above-given program, the inputted data x is to be shifted by 3 bits left side. The answer in the binary bits would be as follows:

|0 | 0| 0| 0| 0| 0| 0| 0| 0| 0| 0| 1| 0| 0| 0| 0|
|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01| 0|

The corresponding decimal number is 16, i.e. answer should be 16.

Shifting three bits left means the number is multiplied by 8; in short y=n*2s where n = number and s = the number of position to be shifted.

As per the program given above,

Y=2*2E3 = 16.

3. Write a program to use bitwise AND operator between the two integers and display the results.

```c 

void main()

{

int a,b,c;

clrscr();

printf(“Read The Integers from keyboard (a & b) :-”);

scanf(“%d %d”, &a,&b);

c=a & b;

printf(“The Answer after ANDing is (C)= %d”,c);

}
```
OUTPUT:
```
Read The Integers from keyboard (a & b) : 8 4

The Answer after ANDing is (C) = 0
```
Binary equivalent of 8 is

Before execution:

a=8

|0 | 0| 0| 0| 0| 0| 0| 0| 0| 0| 0| 0| 1| 0| 0| 0|
|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01| 0|

b=4

Binary equivalent of 4 is

|0 | 0| 0| 0| 0| 0| 0| 0| 0| 0| 0| 0| 0| 1| 0| 0|
|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01| 0|

After execution

c=0

Binary equivalent of 0 is

|0 | 0| 0| 0| 0| 0| 0| 0| 0| 0| 0| 0| 0| 0| 0| 0|
|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01| 0|

OR

Read The Integers through keyboard (a & b) : 8 8

The Answer after ANDing is (C) = 8

Before execution

a=8

Binary equivalent of 8

|0 | 0| 0| 0| 0| 0| 0| 0| 0| 0| 0| 0| 1| 0| 0| 0|
|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01| 0|

Before execution

b=8

Binary equivalent of 8

|0 | 0| 0| 0| 0| 0| 0| 0| 0| 0| 0| 0| 1| 0| 0| 0|
|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01| 0|

After execution

c=8

Binary equivalent of 8

|0 | 0| 0| 0| 0| 0| 0| 0| 0| 0| 0| 0| 1| 0| 0| 0|
|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01| 0|


The table for AND operation (Table A) is as follows and can be used in future for reference. Similarly, the table for OR operator (Table B) can be used as follows.
Table A for AND

<table>
	<tr>
		<th colspan="2">Inputs</th>
		<th>Output</th>
 	</tr>
 	<tr>
  		<td>0</td>
   		<td>0</td>
		<td align="center">0</td>
 	</tr>
     <tr>
  		<td>0</td>
   		<td>1</td>
		<td align="center">0</td>
 	</tr>
     <tr>
  		<td>1</td>
   		<td>0</td>
		<td align="center">0</td>
 	</tr>
     <tr>
  		<td>1</td>
   		<td>1</td>
		<td align="center">1</td>
 	</tr>
</table>
			



Table B for OR operator

<table>
	<tr>
		<th colspan="2">Inputs</th>
		<th>Output</th>
 	</tr>
 	<tr>
  		<td>0</td>
   		<td>0</td>
		<td align="center">0</td>
 	</tr>
     <tr>
  		<td>0</td>
   		<td>1</td>
		<td align="center">1</td>
 	</tr>
     <tr>
  		<td>1</td>
   		<td>0</td>
		<td align="center">1</td>
 	</tr>
     <tr>
  		<td>1</td>
   		<td>1</td>
		<td align="center">1</td>
 	</tr>
</table>

4. Write a program to operate OR operation on two integers and display the result.

 ```c

void main()

{

int a,b,c;

clrscr();

printf(“Read The Integer from keyboard (a & b) :-”);

scanf(“%d %d”, &a,&b);

c=a | b;

printf(“The Oring operation bewteen a & b in c = %d”,c);

getche();

}
```
OUTPUT:
```
Read The Integer from keyboard (a & b) :- 8 4

The Oring operation between a & b in c = 12
```
Before execution

a=8

Binary equivalent of 8

|0 | 0| 0| 0| 0| 0| 0| 0| 0| 0| 0| 0| 1| 0| 0| 0|
|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01| 0|

Before execution

b=4

Binary equivalent of 4

|0 | 0| 0| 0| 0| 0| 0| 0| 0| 0| 0| 0| 0| 1| 0| 0|
|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01| 0|

After execution

c=12

Binary equivalent of 12

|0 | 0| 0| 0| 0| 0| 0| 0| 0| 0| 0| 0| 1| 1| 0| 0|
|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01| 0|

The table for Exclusive OR (XOR) is as follows 

 Table of exclusive OR
 
 <table>
	<tr>
		<th colspan="2">Inputs</th>
		<th>Output</th>
 	</tr>
 	<tr>
  		<td>0</td>
   		<td>0</td>
		<td align="center">0</td>
 	</tr>
     <tr>
  		<td>0</td>
   		<td>1</td>
		<td align="center">1</td>
 	</tr>
     <tr>
  		<td>1</td>
   		<td>0</td>
		<td align="center">1</td>
 	</tr>
     <tr>
  		<td>1</td>
   		<td>1</td>
		<td align="center">0</td>
 	</tr>
</table>

5. Write a program with Exclusive OR Operation between the two integers and display the result.
```c
void main()

{

int a,b,c;

clrscr();

printf(“Read The Integers from keyboard (a & b) :-”);

scanf(“%d %d”, &a,&b);

c=a^b;

printf(“The data after Exclusive OR operation is in c= %d”,c);

getche();

}
```
OUTPUT:
```
Read The Integers from keyboard (a & b) : 8 2

The data after Exclusive OR operation is in c =10
```
Before execution

a=8

|0 | 0| 0| 0| 0| 0| 0| 0| 0| 0| 0| 0| 1| 0| 0| 0|
|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01| 0|

Before execution

b=2

|0 | 0| 0| 0| 0| 0| 0| 0| 0| 0| 0| 0| 0| 0| 1| 0|
|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01| 0|


After execution

c=10

|0 | 0| 0| 0| 0| 0| 0| 0| 0| 0| 0| 0| 1| 0| 1| 0|
|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01| 0|


The table for bitwise complement-operator (Inverter Logic) is as follows Table Below
Table of inverter logic

<table>
	<tr>
		<th>Input (X)</th>
		<th>Output (X)</th>
 	</tr>
 	<tr>
  		<td>0</td>
   		<td>1</td>
 	</tr>
     <tr>
  		<td>1</td>
   		<td>0</td>
 	</tr>
</table>

The operator ~ is used for inverting the bits with this operator 0 becomes 1 & 1 becomes 0.

Write a program to show the effect of ~ operator.
```c
 

void main()

{

unsigned int v=0;

clrscr();

printf(“\n %u”, ~v);

}
```
OUTPUT:
```
65535
```

